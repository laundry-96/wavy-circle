#![allow(unused_imports)]
#![allow(unused_variables)]
#![allow(unused_mut)]
#![allow(dead_code)]
extern crate nannou;

mod omoiirocolors;
mod quadtree;
use crate::omoiirocolors::Palette;
use nannou::prelude::Frame;
use nannou::prelude::*;
use nannou::rand::*;
use rand_xorshift::XorShiftRng;

const LENGTH_FRAME: u64 = 1000;
const START_FRAME: u64 = 0;
const SEED: u64 = 1;
fn main() {
    nannou::app(model).update(update).run();
}

struct Model {
    palette: Palette,
    all_things: Vec<Vec<Thing>>,
    things: Vec<Thing>,
    main: Vec<Thing>,
    frame: u64,
    index: usize,
    rng: XorShiftRng,
    //sim: Fluid,
    pressedleft: bool,
    pressedright: bool,
    px: f32,
    py: f32,
    tree: quadtree::QuadTree<Thing>,
    max_density: f32,
}
struct Thing {
    position: Vector2,
    original_pos: Vector2,
    start_pos: Vector2,
    end_pos: Vector2,
    size: f32,
    side: f32,
    index: usize,
    frac: f32,
    direction: f32,
    done: bool,
    draw: bool,

    speed: Vector2,
    acceleration: Vector2,
    life: i32,
    time: f32,
    val: f32,
    prev_pos: Vector2,
}

impl PartialEq for Thing {
    fn eq(&self, other: &Thing) -> bool {
        self.position == other.position
    }
}
impl quadtree::WithPos for Thing {
    fn get_pos(&self) -> Vector2 {
        self.position
    }
}

impl Thing {
    fn new(x: f32, y: f32, s: f32, i: usize, f: f32, d: f32) -> Self {
        let position = pt2(x, y);
        let size = s;
        let index = i;
        let frac = f;
        let side = if ((f * 20.0) as usize) % 2 == 0 {
            1.0
        } else {
            -1.0
        };

        //let temp= 0.0;
        //let temp2= 0.0;
        let dir = d;
        let speed_vec = vec2(dir.cos(), dir.sin()) * (0.5 + f * 0.5);
        let sp = position + speed_vec * 100.0;
        let ep = position - speed_vec * 100.0;
        Thing {
            position,
            original_pos: pt2(x, y),
            start_pos: sp,
            end_pos: ep,
            size,
            side: side,
            index,
            frac,
            direction: dir,
            done: false,
            draw: false,
            speed: speed_vec,
            acceleration: vec2(0.0, 0.0),
            life: 200,
            time: 0.0,
            val: 0.0,
            prev_pos: pt2(x, y),
        }
    }

    fn intersects(&self, other: &Thing) -> Vec<Vector3> {
        let mut res = Vec::new();
        let d = self.distance(other);
        if d > self.size + other.size {
            //do nothing
        } else if d < (self.size - other.size).abs() {
            //do nothing
        } else {
            let a = (self.size * self.size - other.size * other.size + d * d) / (2.0 * d);
            let h = (self.size * self.size - a * a).sqrt();
            let p2x = self.position.x + a * (other.position.x - self.position.x) / d;
            let p2y = self.position.y + a * (other.position.y - self.position.y) / d;
            let p3x1 = p2x + h * (other.position.y - self.position.y) / d;
            let p3y1 = p2y - h * (other.position.x - self.position.x) / d;
            let p3x2 = p2x - h * (other.position.y - self.position.y) / d;
            let p3y2 = p2y + h * (other.position.x - self.position.x) / d;
            res.push(pt3(p3x1, p3y1, h));
            res.push(pt3(p3x2, p3y2, h));
        }
        res
    }

    fn distance(&self, other: &Thing) -> f32 {
        self.position.distance(other.position)
    }
    fn manhattan(&self, other: &Thing) -> f32 {
        (self.position.x - other.position.x)
            .abs()
            .max((self.position.y - other.position.y).abs())
    }

    fn push(&mut self, other: &Thing, f: f32) {
        let diff = (self.position - other.position).normalize() * f;
        self.acceleration += diff;
    }
    fn towards(&mut self, other: &Thing, f: f32) {
        let to_other = (other.position - self.position).normalize();
        self.position += to_other * f;
    }

    fn pull(&mut self, other: &Thing, f: f32) {
        let diff = (self.position - other.position).normalize() * -f;
        self.acceleration += diff;
    }
    fn pullpt(&mut self, other: &Vector2, f: f32) {
        let diff = (self.position - *other).normalize() * -f;
        self.acceleration += diff;
    }
    fn pushpt(&mut self, other: &Vector2, f: f32) {
        let diff = (self.position - *other).normalize() * f;
        self.acceleration += diff;
    }
    fn rotate(&mut self, da: f32) {
        self.direction += da;
    }
    fn save(&mut self) {
        self.prev_pos.x = self.position.x;
        self.prev_pos.y = self.position.y;
    }
    fn bounds(&mut self, limit: f32) {
        if self.position.x > limit {
            self.position.x = limit;
        }
        if self.position.x < -limit {
            self.position.x = -limit;
        }
        if self.position.y > limit {
            self.position.y = limit;
        }
        if self.position.y < -limit {
            self.position.y = -limit;
        }
    }

    fn wrap(&mut self, limit: f32) {
        if self.position.x > limit {
            self.position.x -= 2.0 * limit;
            self.prev_pos.x = self.position.x;
        }
        if self.position.x < -limit {
            self.position.x += 2.0 * limit;
            self.prev_pos.x = self.position.x;
        }
        if self.position.y > limit {
            self.position.y -= 2.0 * limit;
            self.prev_pos.y = self.position.y;
        }
        if self.position.y < -limit {
            self.position.y += 2.0 * limit;
            self.prev_pos.y = self.position.y;
        }
    }
    fn forward(&mut self, df: f32) {
        //cycle to zone
        self.position.x += self.direction.cos() * df;
        self.position.y += self.direction.sin() * df;
        if self.position.x > 510.0 {
            self.position.x -= 1020.0;
            self.prev_pos.x = self.position.x;
        }
        if self.position.x < -510.0 {
            self.position.x += 1020.0;
            self.prev_pos.x = self.position.x;
        }
        if self.position.y > 510.0 {
            self.position.y -= 1020.0;
            self.prev_pos.y = self.position.y;
        }
        if self.position.y < -510.0 {
            self.position.y += 1020.0;
            self.prev_pos.y = self.position.y;
        }
    }

    fn align(&mut self, other: &Thing, df: f32) {
        //make the direction align with other one
        let mut diff_direction = other.direction - self.direction;
        while diff_direction > PI {
            diff_direction -= TAU;
        }
        while diff_direction < -PI {
            diff_direction += TAU;
        }

        // diff_direction is now between -PI and PI
        self.direction += diff_direction * df;
    }
    fn align_speed(&mut self, other: &Thing, df: f32) {
        let mut angle = self.speed.y.atan2(self.speed.x);
        let angleo = other.speed.y.atan2(other.speed.x);
        let mut diff_direction = angleo - angle;
        while diff_direction > PI {
            diff_direction -= TAU;
        }
        while diff_direction < -PI {
            diff_direction += TAU;
        }
        angle += diff_direction * df;
        let l = self.speed.magnitude();
        self.speed = vec2(angle.cos(), angle.sin()) * l;
    }

    fn rotate_speed(&mut self, da: f32) {
        let mut angle = self.speed.y.atan2(self.speed.x);
        angle += da;
        let l = self.speed.magnitude();
        self.speed = vec2(angle.cos(), angle.sin()) * l;
        //let l = self.speed.magnitude();
        //let mut angle = self.speed.y.atan2(self.speed.x);
    }
    fn recompute(&mut self) {
        let l = self.speed.magnitude();
        self.speed = vec2(self.direction.cos(), self.direction.sin()) * l;
        self.done = true;
    }

    fn update(&mut self, dt: f32, damp: f32) {
        self.done = false;
        self.prev_pos.x = self.position.x;
        self.prev_pos.y = self.position.y;
        self.speed += self.acceleration * dt;
        if self.speed.magnitude() > 3.0 {
            self.speed = self.speed.normalize() * 3.0;
        }
        //recompute speed
        self.position += self.speed * dt;

        self.speed *= damp;
        self.acceleration *= 0.0;

        self.life = (self.life - 1).max(0);
    }
}

fn smoothstep(x: f32) -> f32 {
    x * x * (3.0 - 2.0 * x)
}
fn make_loop(mut fraci: f32) -> f32 {
    while fraci > 1.0 {
        fraci -= 1.0;
    }
    while fraci < 0.0 {
        fraci += 1.0;
    }
    fraci = 2.0 * fraci;
    if fraci > 1.0 {
        fraci = 2.0 - fraci;
    }
    fraci = smoothstep(fraci);
    fraci
}

fn model(app: &App) -> Model {
    let window_id = app
        .new_window()
        .size(1024, 1024)
        //.msaa_samples(4)
        //.with_dimensions(1700,2550/2)
        //.with_title("Daily")
        .view(view)
        .event(window_event)
        .build()
        .unwrap();

    //let s = 1500;
    let s = 1000;
    let mut protec = 0;
    let mut things = Vec::new();
    let mut all_things = Vec::new();
    let palette = Palette::new();

    let mut rng = XorShiftRng::seed_from_u64(SEED);

    // How many circles do we want?
    for i in 0..1 {

        let mut r = 100.0 + 50.0 * (i as f32 + 10.0);
        let dist = TAU * r;

        let mut cirle_points = (dist / 50.0).floor() as usize;
        cirle_points = cirle_points.min(10);
        cirle_points *= 2;

        for k in 0..cirle_points {
            println!("On step {} out of {}", k, cirle_points);
            //while things.len() < s && protec < 100000 {
            //protec += 1;

            let mut r = 350.0; //10.0+2.0* (k as f32);//rng.gen::<f32>().powf(0.5) * max_radius;
            let nsides = 5.0;
            let pin = PI / nsides;
            let taun = TAU / nsides;
            //let mut angle = rng.gen::<f32>().powf(0.5) * TAU;
            let mut angle = TAU * (k as f32) / (cirle_points as f32); //rng.gen::<f32>() * TAU;
                                                               //let mut r = 400.0;//angle*0.1;//rng.gen::<f32>().powf(0.5) * max_radius;
                                                               /*
                                                               if rng.gen::<f32>() < 0.5 {
                                                                   r = -r;
                                                               }
                                                               if rng.gen::<f32>() < 0.5 {
                                                                   angle = -angle;
                                                               }
                                                               */
            //r = 200.0;
            //angle += PI/2.0;
            let x = r * angle.cos();
            let y = r * angle.sin();
            //let mut x = -500.0+15.0+rng.gen::<f32>()*10.0;//(rng.gen::<f32>()-0.5)*1000.0;
            //let mut y = -500.0+15.0+rng.gen::<f32>()*10.0;//(rng.gen::<f32>()-0.5)*1000.0;
            //let mut x = (rng.gen::<f32>()-0.5)*1000.0;
            //let mut y = (rng.gen::<f32>()-0.5)*1000.0;
            //let mut y = (-rng.gen::<f32>())*500.0;
            let d = (x * x + y * y).sqrt() / (2.0.sqrt()) / 500.0;
            let i = rng.gen::<usize>() % palette.len;
            let direction = rng.gen::<f32>() * TAU;
            let dir_vec = vec2(direction.cos(), direction.sin());
            //find start and end for this
            let size = 5.0; // + rng.gen::<f32>().powf(2.0) *2.0;
            angle = y.atan2(x);
            if angle < 0.0 {
                angle += TAU;
            }
            let mut frac = 1.0 - angle.abs() / (TAU); //rng.gen::<f32>();
            if angle < 0.0 {
                frac = 1.0 - frac;
            }
            //let frac = (y+500.0)/1000.0;
            //let frac = rng.gen::<f32>();
            //frac=0.0;

            let mut candidate = Thing::new(x, y, 0.0, i, frac, direction);
            candidate.size = size;
            things.push(candidate);
        }
    }
    println!("{}", things.len());

    let main = Vec::new();

    Model {
        all_things,
        things,
        main,
        palette: palette,
        frame: 0,
        index: 75,
        rng,
        pressedleft: false,
        pressedright: false,
        px: 0.0,
        py: 0.0,
        tree: quadtree::QuadTree::new(),
        max_density: 1.0,
    }
}

fn window_event(app: &App, model: &mut Model, event: WindowEvent) {
    match event {
        KeyPressed(_key) => {}
        KeyReleased(_key) => {}
        MouseMoved(_pos) => {}
        MousePressed(_button) => {}
        MouseReleased(_button) => {}
        MouseEntered => {}
        MouseExited => {}
        MouseWheel(_amount, _phase) => {}
        Moved(_pos) => {}
        Resized(_size) => {}
        Touch(_touch) => {}
        TouchPressure(_pressure) => {}
        HoveredFile(_path) => {}
        DroppedFile(_path) => {}
        HoveredFileCancelled => {}
        Focused => {}
        Unfocused => {}
        Closed => {}
    }
}

fn update(app: &App, model: &mut Model, _update: Update) {
    let frac = ((app.elapsed_frames() + 1 % 450) as f32) / (450 as f32);

    let mut i = 0;
    while i != model.things.len() {
        if model.things[i].life <= 0 {
            model.things.remove(i);
        } else {
            i += 1;
        }
    }

    model.tree = quadtree::QuadTree::new();
    for i in 0..model.things.len() {
        model.tree.insert(&model.things, i);
    }

    let frac_speed = ((app.elapsed_frames() as f32) / (200.0)).sqrt().min(1.0);
    let speed_factor = 100.0 * frac_speed;

    //push away
    for i in 0..model.things.len() {
        let indices = model.tree.get_elements(
            &model.things,
            model.things[i].position.x,
            model.things[i].position.y,
            50.0,
        );
        let (left, right) = model.things.split_at_mut(i + 1);
        for index in 0..indices.len() {
            let j = indices[index];
            if j > i {
                let k = j - i - 1;
                let d = left[i].position.distance(right[k].position);
                if d < 20.0 {
                    let scale = 1.0 - (d / 20.0).powf(1.0);
                    left[i].towards(&right[k], -1.2 * scale);
                    right[k].towards(&left[i], -1.2 * scale);
                }
            }
        }
    }

    let mut i = 0;
    while i != model.things.len() {
        let i1 = (i + 1) % model.things.len();
        let d = model.things[i].position.distance(model.things[i1].position);
        if d > 10.0 {
            if i != model.things.len() - 1 {
                let (left, right) = model.things.split_at_mut(i + 1);
                //add a bit of tension
                left[i].towards(&right[0], 5.1);
                right[0].towards(&left[i], 5.1);
            } else {
                let (left, right) = model.things.split_at_mut(1);
                left[0].towards(&right[right.len() - 1], 5.1);
                right[right.len() - 1].towards(&left[0], 5.1);
            }
        }
        if d > 12.0 {
            let newp = model.things[i].position * 0.5 + model.things[i1].position * 0.5;
            let mut fc = model.things[i].frac * 0.5 + model.things[i1].frac * 0.5;
            if model.things[i].frac == 0.0 && model.things[i1].frac == 1.0 {
                fc = 0.0;
            }
            if model.things[i1].frac == 0.0 && model.things[i].frac == 1.0 {
                fc = 0.0;
            }
            let candidate = Thing::new(newp.x, newp.y, 0.0, 0, fc, 1.0);
            model.things.insert(i1, candidate);
        }
        model.things[i].bounds(500.0);
        if d < 2.0 && model.things.len() > 4 {
            model.things.remove(i);
        } else {
            i += 1;
        }
    }
}

fn view(app: &App, model: &Model, frame: Frame) {
    // Prepare to draw.
    let draw = app.draw();

    // Clear the background
    if app.elapsed_frames() <= 1 {
        draw.background().color(BLACK);
    }

    let mut c: Srgba = srgba(0.0, 0.0, 0.0, 0.0);
    if app.elapsed_frames() > LENGTH_FRAME - 50 {
        c.alpha = 0.3;
        c.alpha = 0.06;
    } else {
        c.alpha = 0.06;
    }
    draw.rect().w_h(1024.0, 1024.0).color(c);
    for i in 0..model.things.len() {
        let i1 = (i + 1) % model.things.len();
        let mut c: Rgba = model
            .palette
            .somecolor_frac_interpol(model.things[i].frac)
            .into();
        c.alpha = 0.6;
        let mut c2: Rgba = model
            .palette
            .somecolor_frac_interpol(model.things[i1].frac)
            .into();
        c2.alpha = 0.6;
        let v0 = (model.things[i].position, c);
        let v1 = (model.things[i1].position, c2);
        let points = vec![v0, v1];
        draw.polyline().stroke_weight(2.0).points_colored(points);
    }

    // Write to the window frame.
    draw.to_frame(app, &frame).unwrap();
}
