//use nannou::prelude::*;
use nannou::color::Mix;
use nannou::color::Srgb;
use nannou::color::Rgb;
use nannou::color::LinSrgb;

pub struct Palette {
    pub colors: Vec<Rgb>,
    pub len: usize,
}

impl Palette {
    pub fn new() -> Self {
        //pink green sky
        let colors: [u32;11] = [0xFFB9CED1,0xFF27A2AA,0xFF037D8A,0xFF035A6B,0xFF80BDC3,0xFFFCC5C1,0xFFE2D3D3,0xFFF89E98,0xFFFFE0AE,0xFFFEBF9F,0xFF252627];
        let colorsv = colors.to_vec();

        //do the conversion myself
        let colsrgb : Vec<Rgb> = colorsv.into_iter().map(|c| {
            let blue: u8 = (c & 0xFF) as u8;
            let green: u8 = (( c >> 8 ) & 0xFF) as u8;
            let red: u8 = (( c >> 16 ) & 0xFF) as u8;
            let r = red as f32 / 255.0;
            let g = green as f32 / 255.0;
            let b = blue as f32 / 255.0;

             Srgb::new(r, g, b)
        }).collect();
    
        
        
        let len = colors.len();
        Palette {colors : colsrgb, len: len}
    }

    pub fn somecolor_mod(&self, index:usize) -> Rgb {
        self.colors[index % self.colors.len()]
    }
    pub fn somecolor_frac(&self, mut frac:f32) -> Rgb {
        while frac < 0.0 {
            frac += 1.0;
        }
        while frac >= 1.0 {
            frac -= 1.0;
        }
        
        let index = (frac*self.colors.len() as f32 ) as usize;
        self.colors[index]
    }
    pub fn somecolor_frac_interpol(&self, mut frac:f32) -> Rgb {
        while frac < 0.0 { frac += 1.0; }
        while frac >= 1.0 { frac -= 1.0; }
        let index = (frac*(self.colors.len()) as f32 ).floor() as usize;
        let mut index2 = index+1;
        if index2 > self.colors.len()-1 {
            index2 = 0
        }
        let frac_remainder = (frac*(self.colors.len()) as f32)-(index as f32);
        let a: LinSrgb = self.colors[index].into_linear();
        let b: LinSrgb = self.colors[index2].into_linear();
        Srgb::from_linear(a.mix(&b, frac_remainder))
    }
}
